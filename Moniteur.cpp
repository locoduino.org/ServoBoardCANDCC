/*=======================================================================================
 * Moniteur.cpp
 *---------------------------------------------------------------------------------------
 * Logiciel pour la carte 8 servos CAN + DCC
 *
 * LOCODUINO, http://www.locoduino.org
 *
 * Auteur : Jean-Luc B\'echennec
 *
 * Ce logiciel est distribu\'e sous la licence GNU Public v2 (GPLv2)
 *
 * Please read the LICENCE file
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
 
#include "Moniteur.h"
#include "CommandInterpreter.h"
#include "HardwareSerial.h"
#include "Configuration.h"
#include "EEPROM.h"

/*
 * Version du logiciel
 */
const byte versionMajeure = 1;
const byte versionMineure = 0;
const byte revision = 1;

const char ouiArgument[] PROGMEM = "oui";
const char nonArgument[] PROGMEM = "non";

/*
 * Flag indiquant si le demarrage a ete effectue en mode moniteur
 */
static bool moniteurEnRoute = false;

static void nombreDArgumentsIncorrect(HardwareSerial &serial, byte minimum, byte maximum)
{
  serial.print(F("Nombre d'arguments incorrect, "));
  serial.print(F("entre "));
  serial.print(minimum);
  serial.print(F(" et "));
  serial.print(maximum);
  serial.println(F(" attendus"));
}

/*
 * Moniteur de commandes
 */
CommandInterpreter interpreter(Serial);

/*---------------------------------------------------------------------------------------
 * Commande d'affichage de la version du logiciel
 */
void afficheVersion(CommandInterpreter &cmdInt, byte nbArgs)
{
  if (nbArgs == 0) {
    cmdInt.Serial().print(versionMajeure);
    cmdInt.Serial().print('.');
    cmdInt.Serial().print(versionMineure);
    cmdInt.Serial().print('.');
    cmdInt.Serial().println(revision);
  }
  else cmdInt.Serial().println(F("Cette commande ne prend pas d'argument"));
}

Command afficheVersionCmd("ver", afficheVersion, "Affiche la version du logiciel");

/*---------------------------------------------------------------------------------------
 * Commande de manipulation de l'expandeur de fin de course
 */
void commandeExpandeur(CommandInterpreter &cmdInt, byte nbArgs)
{
  bool erreur = false;
  switch (nbArgs)
  {
    case 0:
      /* lecture de l'activation de l'expandeur */
      if (config::expandeurActif()) cmdInt.Serial().println(F("oui"));
      else cmdInt.Serial().println(F("non"));
      break;
    case 1:
      char *argument;
      if (cmdInt.readString(argument))
        if (strcmp_P(argument, ouiArgument) == 0) config::fixeExpandeur(true);
        else if (strcmp_P(argument, nonArgument) == 0) config::fixeExpandeur(false);
        else {
          erreur = true;
        }
      break;
    default:
      nombreDArgumentsIncorrect(cmdInt.Serial(), 0, 1);
      erreur = true;
      break;
  }
  if (erreur) cmdInt.Serial().println(F("Usage: exp <oui|non>"));
}

Command gereExpandeur("exp", commandeExpandeur, "Affiche ou fixe l'etat de l'expandeur");

/*---------------------------------------------------------------------------------------
 * Commande de manipulation du CAN
 */
void commandeCAN(CommandInterpreter &cmdInt, byte nbArgs)
{
  bool erreur = false;
  switch (nbArgs)
  {
    case 0:
      /* lecture de l'activation du CAN */
      if (config::CANActif()) cmdInt.Serial().println(F("oui"));
      else cmdInt.Serial().println(F("non"));
      break;
    case 1:
      char *argument;
      if (cmdInt.readString(argument))
        if (strcmp_P(argument, ouiArgument) == 0) config::fixeCAN(true);
        else if (strcmp_P(argument, nonArgument) == 0) config::fixeCAN(false);
        else {
          erreur = true;
        }
      break;
    default:
      nombreDArgumentsIncorrect(cmdInt.Serial(), 0, 1);
      erreur = true;
      break;
  }
  if (erreur) cmdInt.Serial().println(F("Usage: can <oui|non>"));  
}

Command gereCAN("can", commandeCAN, "Affiche ou fixe l'etat du CAN");

/*---------------------------------------------------------------------------------------
 * Commande de manipulation du DCC
 */
void commandeDCC(CommandInterpreter &cmdInt, byte nbArgs)
{
  bool erreur = false;
  switch (nbArgs)
  {
    case 0:
      /* lecture de l'activation du DCC */
      if (config::DCCActif()) cmdInt.Serial().println(F("oui"));
      else cmdInt.Serial().println(F("non"));
      break;
    case 1:
      char *argument;
      if (cmdInt.readString(argument))
        if (strcmp_P(argument, ouiArgument) == 0) config::fixeDCC(true);
        else if (strcmp_P(argument, nonArgument) == 0) config::fixeDCC(false);
        else {
          erreur = true;
        }
      else erreur = true;
      break;
    default:
      nombreDArgumentsIncorrect(cmdInt.Serial(), 0, 1);
      erreur = true;
      break;
  }
  if (erreur) cmdInt.Serial().println(F("Usage: dcc <oui|non>"));  
}

Command gereDCC("dcc", commandeDCC, "Affiche ou fixe l'etat du DCC");

/*---------------------------------------------------------------------------------------
 * Commande de manipulation de l'adresse CAN
 */
void commandeAdresseCAN(CommandInterpreter &cmdInt, byte nbArgs)
{
  bool erreur = false;
  int adresse;
  switch (nbArgs)
  {
    case 0:
      /* lecture de l'adresse du CAN */
      adresse = config::adresseCAN();
      if (adresse == -1) cmdInt.Serial().println(F("Adresse CAN non fixee"));
      else cmdInt.Serial().println(adresse);
      break;
    case 1:
      if (cmdInt.readInt(adresse)) config::fixeAdresseCAN(adresse);
      else erreur = true;
      break;
    default:
      nombreDArgumentsIncorrect(cmdInt.Serial(), 0, 1);
      erreur = true;
      break;
  }
  if (erreur) cmdInt.Serial().println(F("Usage: canad <adresse>"));  
}

Command gereAdresseCAN("canad", commandeAdresseCAN, "Affiche ou fixe l'adresse CAN");

/*---------------------------------------------------------------------------------------
 * Commande de manipulation de l'adresse DCC
 */
void commandeAdresseDCC(CommandInterpreter &cmdInt, byte nbArgs)
{
  bool erreur = false;
  int adresse;
  switch (nbArgs)
  {
    case 0:
      /* lecture de l'adresse du CAN */
      adresse = config::adresseDCC();
      if (adresse == -1) cmdInt.Serial().println(F("Adresse DCC non fixee"));
      else cmdInt.Serial().println(adresse);
      break;
    case 1:
      if (cmdInt.readInt(adresse)) config::fixeAdresseDCC(adresse);
      else erreur = true;
      break;
    default:
      nombreDArgumentsIncorrect(cmdInt.Serial(), 0, 1);
      erreur = true;
      break;
  }
  if (erreur) cmdInt.Serial().println(F("Usage: dccad <adresse>"));  
}

Command gereAdresseDCC("dccad", commandeAdresseDCC, "Affiche ou fixe l'adresse DCC");

/*---------------------------------------------------------------------------------------
 * Commande de manipulation de la retrosignalisation
 */
void commandeRetro(CommandInterpreter &cmdInt, byte nbArgs)
{
  bool erreur = false;
  int identifiant;
  int periode;
  switch (nbArgs)
  {
    case 0:
      if (config::retroActif()) {
        identifiant = config::identifiantTrameRetro();
        periode = config::periodeEmissionRetro();
        if (identifiant == -1) cmdInt.Serial().println(F("Identifiant non fixe"));
        else {
          cmdInt.Serial().print(F("oui id="));
          cmdInt.Serial().print(identifiant);
          cmdInt.Serial().print(F(" periode="));
          cmdInt.Serial().print(periode * 10);
          cmdInt.Serial().println(F("ms"));
        }          
      }
      else cmdInt.Serial().println(F("non"));
      break;
    case 1:
      char *argument;
      if (cmdInt.readString(argument))
        if (strcmp_P(argument, nonArgument) == 0) config::fixePeriodeRetro(0);
        else erreur = true;
      else erreur = true;
      break;
    case 2:
      if (cmdInt.readInt(identifiant))
        if (cmdInt.readInt(periode)) {
          if (periode == 0 || periode % 10 != 0) {
            cmdInt.Serial().println(F("La periode doit etre un multiple de 10"));
            erreur = true;
          }
          else {
            config::fixeIdentifiantTrameRetro(identifiant);
            config::fixePeriodeRetro(periode / 10);
          }
        }
        else erreur = true;
      else erreur = true;
      break;
    default:
      nombreDArgumentsIncorrect(cmdInt.Serial(), 0, 2);
      break;
  }
  if (erreur) {
    cmdInt.Serial().println(F("Usage: retro non"));  
    cmdInt.Serial().println(F("       retro <id> <periode>"));  
  }
}

Command gereRetro("retro", commandeRetro, "Affiche ou fixe les parametres de la retrosignalisation");

void printHexAddress(HardwareSerial &serial, unsigned int address)
{
  if (address < 16) serial.print('0');
  if (address < 256) serial.print('0');
  serial.print(address, HEX);
  serial.print(": ");
}

void EEPROMdump(CommandInterpreter &cmdInt, byte argCount)
{
  HardwareSerial &serial = cmdInt.Serial();
  unsigned int startAddress;
  unsigned int length;
  bool printHelp = false;
  
  switch (argCount) {

    case 1:
      if (cmdInt.readUnsignedInt(startAddress)) {
        printHexAddress(serial, startAddress);
        byte value = EEPROM.read(startAddress);
        if (value < 16) serial.print('0');
        serial.println(value, HEX);
      }
      else printHelp = true;
      break;
      
    case 2:
      if (cmdInt.readUnsignedInt(startAddress) && cmdInt.readUnsignedInt(length)) {
        unsigned int address = startAddress;
        unsigned int counter = 0;
        printHexAddress(serial, address);
        while (counter < length && address <= E2END) {
          byte value = EEPROM.read(address++);
          if (value < 16) serial.print('0');
          serial.print(value, HEX);
          serial.print(' ');
          if (++counter % 16 == 0 && counter < length) {
            serial.println();
            printHexAddress(serial, address);
          }
        } 
        serial.println();
      }
      else printHelp = true;
      break;
    
    default:
      serial.println(F("Nombre d'arguments incorrect"));
      printHelp = true;
      break;
  }
  if (printHelp) serial.println(F("Usage : rep adresseDebut <longueur> ou rep addresse"));
}

Command dumpEEPROM("rep", EEPROMdump, "Affiche le contenu de l'EEPROM");

/*---------------------------------------------------------------------------------------
 * Demarre le moniteur lorsqu'on est en mode configuration
 */
void demarreMoniteur()
{
  Serial.begin(115200);
  Serial.print('\f');
  Serial.println(F("*--------------------------*"));
  Serial.println(F("*  Carte 8 servos CAN/DCC  *"));
  Serial.println(F("*     Licence GPL v2.0     *"));
  Serial.println(F("* http://www.locoduino.org *"));
  Serial.println(F("*--------------------------*"));
  interpreter.setPrompt(F("Servo>"));
  interpreter.addCommand(afficheVersionCmd);
  interpreter.addCommand(gereExpandeur);
  interpreter.addCommand(gereCAN);
  interpreter.addCommand(gereDCC);
  interpreter.addCommand(gereAdresseCAN);
  interpreter.addCommand(gereAdresseDCC);
  interpreter.addCommand(gereRetro);
  interpreter.addCommand(dumpEEPROM);
  moniteurEnRoute = true;
}

/*---------------------------------------------------------------------------------------
 * Assure la communication du moniteur. A appeler dans loop
 */
void moniteur()
{
  if (moniteurEnRoute) CommandInterpreter::update();
}

