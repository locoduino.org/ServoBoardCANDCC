/*=======================================================================================
 * Configuration.h
 *---------------------------------------------------------------------------------------
 * Logiciel pour la carte 8 servos CAN + DCC
 *
 * LOCODUINO, http://www.locoduino.org
 *
 * Auteur : Jean-Luc B\'echennec
 *
 * Ce logiciel est distribu\'e sous la licence GNU Public v2 (GPLv2)
 *
 * Please read the LICENCE file
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef __CONFIGURATION_H__
#define __CONFIGURATION_H__

#include <Arduino.h>

/*---------------------------------------------------------------------------------------
 * Fonction de lecture et d'écriture de la configuration
 */

namespace config {
  /* 
   * expandeurActif ertourne true si l'expandeur de fins de course est employé,
   * false sinon.
   */
  bool expandeurActif();

  /*
   * détermine l'emploi de l'expandeur de fins de course
   */
  void fixeExpandeur(const bool actif);

  /* 
   * CANActif retourne true si le bus CAN est employé, false sinon
   */
  bool CANActif();

  /*
   * détermine l'emploi du CAN
   */
  void fixeCAN(const bool actif);       

  /*
   * DCCActif retourne true si le bus DCC est employé, false sinon
   */
  bool DCCActif();       

  /* 
   * détermine l'emploi du DCC
   */
  void fixeDCC(const bool actif);

  /*
   * adresseCAN retourne l'adresse CAN de la carte
   */
  int adresseCAN();

  /*
   * détermine l'adresse CAN de la carte
   */
  void fixeAdresseCAN(const int adresse);

  /*
   * adresseDCC retourne l'adresse DCC de la carte
   */
  int adresseDCC();
  
  /*
   * détermine l'adresse DCC de la carte
   */
  void fixeAdresseDCC(const int adresse);

  /*
   * retroActif retourne true si l'envoi des trames CAN de retrosignalisation
   * est actif, false sinon 
   */
  bool retroActif();
  
  /*
   * retourne la période d'envoi des trames de rétrosignalisation
   */
  byte periodeEmissionRetro();

  /*
   * détermine l'emploi des trames de rétrosignaliation en dizaines de ms. 
   * Si la periode est différente de 0, la période d'envoi des trames est fixée.
   * 1 donnera une période de 10ms et 254 de 2540 ms (2,54s)
   * Si la période est 0, 255 ou omise, les trames ne sont pas envoyées
   */
  void fixePeriodeRetro(const byte periode = 0);

  /*
   * retourne l'identifiant des trames de rétrosignalisation
   */
  int identifiantTrameRetro();
  
  /*
   * détermine l'identifiant des trames de retrosignalisation
   */
  void fixeIdentifiantTrameRetro(const int identifiant);

  /*
   * retourne la position minimum en largeur de pulse du servo numServo (0 à 7)
   */
  int positionMinServo(const byte numeroServo);

  /*
   * détermine la position minimum en largeur de pulse du servo numServo (0 à 7)
   */
  void fixePositionMinServo(const byte numeroServo, const int pos);

  /*
   * retourne la position maximum en largeur de pulse du servo numServo (0 à 7)
   */
  int positionMaxServo(const byte numeroServo);

  /*
   * détermine la position maximum en largeur de pulse du servo numServo (0 à 7)
   */
  void fixePositionMaxServo(const byte numeroServo, const int pos);

  /*
   * retourne la vitesse de min vers max du servo numServo (0 à 7) 
   */
  float vitesseServoMinMax(const byte numeroServo);

  /*
   * détermine la vitesse de min vers max du servo numServo (0 à 7) 
   */
  void fixeVitesseServoMinMax(const byte numeroServo, const float vitesse);

  /*
   * retourne la vitesse de max vers min du servo numServo (0 à 7)
   */
  float vitesseServoMaxMin(const byte numeroServo);

  /*
   * détermine la vitesse de max vers min du servo numServo (0 à 7)
   */
  void fixeVitesseServoMaxMin(const byte numeroServo, const float vitesse);
}

#endif
