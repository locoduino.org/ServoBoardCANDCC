/*=======================================================================================
 * ServoBoardCANDCC.ino
 *---------------------------------------------------------------------------------------
 * Logiciel pour la carte 8 servos CAN + DCC
 *
 * LOCODUINO, http://www.locoduino.org
 *
 * Auteur : Jean-Luc B\'echennec
 *
 * Ce logiciel est distribu\'e sous la licence GNU Public v2 (GPLv2)
 *
 * Please read the LICENCE file
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
#include <CommandInterpreter.h>

#include "Moniteur.h"
#include "Broches.h"

void setup()
{
  /* Lecture du bouton CFG pour déterminer si on rentre en mode moniteur */
  pinMode(pinBoutonCFG, INPUT_PULLUP);
  if (digitalRead(pinBoutonCFG) == LOW) {
    /* 
     * Fait clignoter la LED pour indiquer que l'on est 
     * en attente du relachement du bouton CFG
     */
    pinMode(pinLED, OUTPUT);
    digitalWrite(pinLED, HIGH);
    while(digitalRead(pinBoutonCFG) == LOW) {
      delay(50);
      digitalWrite(pinLED, ! digitalRead(pinLED));
    }
    digitalWrite(pinLED, LOW);
    pinMode(pinLED, INPUT);

    /* mode moniteur */
    demarreMoniteur();
  }
}

void loop()
{
  /*
   * fait fonctionner le moniteur. Quand on n'est pas en
   */
  moniteur();
}

