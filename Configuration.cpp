/*=======================================================================================
 * Configuration.cpp
 *---------------------------------------------------------------------------------------
 * Logiciel pour la carte 8 servos CAN + DCC
 *
 * LOCODUINO, http://www.locoduino.org
 *
 * Auteur : Jean-Luc B\'echennec
 *
 * Ce logiciel est distribu\'e sous la licence GNU Public v2 (GPLv2)
 *
 * Please read the LICENCE file
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "Configuration.h"
#include <EEPROM.h>

/*
 * Cartographie EEPROM des donnees de configuration
 */
static const word EXPANDEUR_F   =  0;    /* Expandeur de fin de course on/off          */
static const word CAN_F         =  1;    /* CAN on/off                                 */
static const word DCC_F         =  2;    /* DCC on/off                                 */
static const word CAN_ADRESSE   =  3;    /* Adresse CAN de la carte poids fort en 3    */
                                         /* poids faible en 4                          */
static const word DCC_ADRESSE   =  5;    /* Adresse DCC de la carte poids fort en 5    */
                                         /* poids faible en 6                          */
static const word PERIODE_RETRO =  7;    /* periode d'emission des trames de           */
                                         /* retrosignalisation en dizaine de ms        */
                                         /* 0 = pas de retrosignalisation              */
static const word ID_RETRO      =  8;    /* Id des trames de retro poids fort en 8     */
                                         /* poids faible en 9                          */
                                      
static const word S0_MIN        = 0x10;  /* Position min du servo 0, poids fort en 10h */
                                         /* poids faible en 11h                        */
static const word S0_MAX        = 0x12;  /* Position max du servo 0, poids fort en 12h */
                                         /* poids faible en 13h                        */
static const word S0_V_MIN_MAX  = 0x14;  /* Vitesse de min vers max,                   */
                                         /* flottant sur 4 octets : 14h à 17h          */
static const word S0_V_MAX_MIN  = 0x18;  /* Vitesse de max vers min,                   */
                                         /* flottant sur 4 octets : 18h à 1Bh          */
                                      
static const word S1_MIN        = 0x20;  /* Position min du servo 1, poids fort en 20h */
                                         /* poids faible en 21h                        */
static const word S1_MAX        = 0x22;  /* Position max du servo 1, poids fort en 22h */
                                         /* poids faible en 23h                        */
static const word S1_V_MIN_MAX  = 0x24;  /* Vitesse de min vers max,                   */
                                         /* flottant sur 4 octets : 24h à 27h          */
static const word S1_V_MAX_MIN  = 0x28;  /* Vitesse de max vers min,                   */
                                         /* flottant sur 4 octets : 28h à 2Bh          */
                                      
static const word S2_MIN        = 0x30;  /* Position min du servo 2, poids fort en 30h */
                                         /* poids faible en 31h                        */
static const word S2_MAX        = 0x32;  /* Position max du servo 2, poids fort en 32h */
                                         /* poids faible en 33h                        */
static const word S2_V_MIN_MAX  = 0x34;  /* Vitesse de min vers max,                   */
                                         /* flottant sur 4 octets : 34h à 37h          */
static const word S2_V_MAX_MIN  = 0x38;  /* Vitesse de max vers min,                   */
                                         /* flottant sur 4 octets : 38h à 3Bh          */
                                      
static const word S3_MIN        = 0x40;  /* Position min du servo 3, poids fort en 40h */
                                         /* poids faible en 41h                        */
static const word S3_MAX        = 0x42;  /* Position max du servo 3, poids fort en 42h */
                                         /* poids faible en 43h                        */
static const word S3_V_MIN_MAX  = 0x44;  /* Vitesse de min vers max,                   */
                                         /* flottant sur 4 octets : 44h à 47h          */
static const word S3_V_MAX_MIN  = 0x48;  /* Vitesse de max vers min,                   */
                                         /* flottant sur 4 octets : 48h à 4Bh          */
                                      
static const word S4_MIN        = 0x50;  /* Position min du servo 4, poids fort en 50h */
                                         /* poids faible en 51h                        */
static const word S4_MAX        = 0x52;  /* Position max du servo 4, poids fort en 52h */
                                         /* poids faible en 53h                        */
static const word S4_V_MIN_MAX  = 0x54;  /* Vitesse de min vers max,                   */
                                         /* flottant sur 4 octets : 54h à 57h          */
static const word S4_V_MAX_MIN  = 0x58;  /* Vitesse de max vers min,                   */
                                         /* flottant sur 4 octets : 58h à 5Bh          */
                                      
static const word S5_MIN        = 0x60;  /* Position min du servo 5, poids fort en 60h */
                                         /* poids faible en 61h                        */
static const word S5_MAX        = 0x62;  /* Position max du servo 5, poids fort en 62h */
                                         /* poids faible en 63h                        */
static const word S5_V_MIN_MAX  = 0x64;  /* Vitesse de min vers max,                   */
                                         /* flottant sur 4 octets : 64h à 67h          */
static const word S5_V_MAX_MIN  = 0x68;  /* Vitesse de max vers min,                   */
                                         /* flottant sur 4 octets : 68h à 6Bh          */
                                      
static const word S6_MIN        = 0x70;  /* Position min du servo 6, poids fort en 70h */
                                         /* poids faible en 71h                        */
static const word S6_MAX        = 0x72;  /* Position max du servo 6, poids fort en 72h */
                                         /* poids faible en 73h                        */
static const word S6_V_MIN_MAX  = 0x74;  /* Vitesse de min vers max,                   */
                                         /* flottant sur 4 octets : 74h à 77h          */
static const word S6_V_MAX_MIN  = 0x78;  /* Vitesse de max vers min,                   */
                                         /* flottant sur 4 octets : 78h à 7Bh          */
                                      
static const word S7_MIN        = 0x80;  /* Position min du servo 7, poids fort en 80h */
                                         /* poids faible en 81h                        */
static const word S7_MAX        = 0x82;  /* Position max du servo 7, poids fort en 82h */
                                         /* poids faible en 83h                        */
static const word S7_V_MIN_MAX  = 0x84;  /* Vitesse de min vers max,                   */
                                         /* flottant sur 4 octets : 84h à 87h          */
static const word S7_V_MAX_MIN  = 0x88;  /* Vitesse de max vers min,                   */
                                         /* flottant sur 4 octets : 88h à 8Bh          */

static const word MASQUE_ACTIFS = 0x100; /* bit a 1 = servo actif (en mouvement)       */
static const word POS_INACTIFS  = 0x101; /* bit a 0 = servo a min, 1 = servo a max     */
static const word POS_ACTIF_0   = 0x102; /* position du servo 0 si actif               */
static const word POS_ACTIF_1   = 0x103; /* position du servo 1 si actif               */
static const word POS_ACTIF_2   = 0x103; /* position du servo 2 si actif               */
static const word POS_ACTIF_3   = 0x105; /* position du servo 3 si actif               */
static const word POS_ACTIF_4   = 0x106; /* position du servo 4 si actif               */
static const word POS_ACTIF_5   = 0x107; /* position du servo 5 si actif               */
static const word POS_ACTIF_6   = 0x108; /* position du servo 6 si actif               */
static const word POS_ACTIF_7   = 0x109; /* position du servo 7 si actif               */

namespace config {
  /* 
   * expandeurActif retourne true si l'expandeur de fins de course est employé,
   * false sinon.
   */
  bool expandeurActif()
  {
    /* toute valeur différente de 1 est false */
    byte actif = EEPROM.read(EXPANDEUR_F);
    return (actif == 1);
  }

  /*
   * détermine l'emploi de l'expandeur de fins de course
   */
  void fixeExpandeur(const bool actif)
  {
    byte valeurActif = actif ? 1 : 0 ;
    EEPROM.update(EXPANDEUR_F, valeurActif);
  }

  /* 
   * CANActif retourne true si le bus CAN est employé, false sinon
   */
  bool CANActif()
  {
    /* toute valeur différente de 1 est false */
    byte actif = EEPROM.read(CAN_F);
    return (actif == 1);
  }

  /*
   * détermine l'emploi du CAN
   */
  void fixeCAN(const bool actif)
  {
    byte valeurActif = actif ? 1 : 0 ;
    EEPROM.update(CAN_F, valeurActif);
  }

  /*
   * DCCActif retourne true si le bus DCC est employé, false sinon
   */
  bool DCCActif()
  {
    /* toute valeur différente de 1 est false */
    byte actif = EEPROM.read(DCC_F);
    return (actif == 1);
  }

  /* 
   * détermine l'emploi du DCC
   */
  void fixeDCC(const bool actif)
  {
    byte valeurActif = actif ? 1 : 0 ;
    EEPROM.update(DCC_F, valeurActif);
  }

  /*
   * adresseCAN retourne l'adresse CAN de la carte
   */
  int adresseCAN()
  {
    int adresse;
    EEPROM.get(CAN_ADRESSE, adresse);
    return adresse;
  }

  /*
   * détermine l'adresse CAN de la carte
   */
  void fixeAdresseCAN(const int adresse)
  {
    EEPROM.put(CAN_ADRESSE, adresse);
  }

  /*
   * adresseDCC retourne l'adresse DCC de la carte
   */
  int adresseDCC()
  {
    int adresse;
    EEPROM.get(DCC_ADRESSE, adresse);
    return adresse;
  }
  
  /*
   * détermine l'adresse DCC de la carte
   */
  void fixeAdresseDCC(const int adresse)
  {
    EEPROM.put(DCC_ADRESSE, adresse);
  }

  /*
   * retroActif retourne true si l'envoi des trames CAN de retrosignalisation
   * est actif, false sinon 
   */
  bool retroActif()
  {
    byte retro = EEPROM.read(PERIODE_RETRO);
    return !(retro == 0 || retro == 255);
  }
  
  /*
   * retourne la période d'envoi des trames de rétrosignalisation, 0 signifie
   * pas de retro
   */
  byte periodeEmissionRetro()
  {
    byte periode = EEPROM.read(PERIODE_RETRO);
    if (periode == 255) periode = 0;
    return periode;
  }

  /*
   * détermine l'emploi des trames de rétrosignaliation en dizaines de ms. 
   * Si la periode est différente de 0, la période d'envoi des trames est fixée.
   * 1 donnera une période de 10ms et 255 de 2550 ms (2,55s)
   * Si la période est 0, 255 ou omise, les trames ne sont pas envoyées
   */
  void fixePeriodeRetro(const byte periode)
  {
    EEPROM.put(PERIODE_RETRO, periode);
  }

  /*
   * retourne l'identifiant des trames de rétrosignalisation, -1 signifie pas
   * d'identifiant défini
   */
  int identifiantTrameRetro()
  {
    int identifiant;
    EEPROM.get(ID_RETRO, identifiant);
    return identifiant;
  }
  
  /*
   * détermine l'identifiant des trames de retrosignalisation
   */
  void fixeIdentifiantTrameRetro(const int identifiant)
  {
    EEPROM.put(ID_RETRO, identifiant);
  }

  /*
   * retourne la position minimum en largeur de pulse du servo numServo (0 à 7)
   */
  int positionMinServo(const byte numeroServo)
  {
    int pos;
    if (numeroServo < 8) {
      int adresse = (numeroServo << 4) + S0_MIN;
      EEPROM.get(adresse, pos);
    }
    else pos = -1;
    
    return pos;
  }

  /*
   * détermine la position minimum en largeur de pulse du servo numServo (0 à 7)
   */
  void fixePositionMinServo(const byte numeroServo, const int pos)
  {
    if (numeroServo < 8) {
      int adresse = (numeroServo << 4) + S0_MIN;
      EEPROM.put(adresse, pos);
    }
  }

  /*
   * retourne la position maximum en largeur de pulse du servo numServo (0 à 7)
   */
  int positionMaxServo(const byte numeroServo)
  {
    int pos;
    if (numeroServo < 8) {
      int adresse = (numeroServo << 4) + S0_MAX;
      EEPROM.get(adresse, pos);
    }
    else pos = -1;
    
    return pos;
  }

  /*
   * détermine la position maximum en largeur de pulse du servo numServo (0 à 7)
   */
  void fixePositionMaxServo(const byte numeroServo, const int pos)
  {
    if (numeroServo < 8) {
      int adresse = (numeroServo << 4) + S0_MAX;
      EEPROM.put(adresse, pos);
    }
  }

  /*
   * retourne la vitesse de min vers max du servo numServo (0 à 7) 
   */
  float vitesseServoMinMax(const byte numeroServo)
  {
    float vitesse;
    if (numeroServo < 8) {
      int adresse = (numeroServo << 4) + S0_V_MIN_MAX;
      long int vitesseBrute;
      EEPROM.get(adresse, vitesseBrute);
      if (vitesseBrute == -1) vitesse = 0.0;
      else EEPROM.get(adresse, vitesse);
    }
    else vitesse = 0.0;
    
    return vitesse;
  }

  /*
   * détermine la vitesse de min vers max du servo numServo (0 à 7) 
   */
  void fixeVitesseServoMinMax(const byte numeroServo, const float vitesse)
  {
   if (numeroServo < 8) {
      int adresse = (numeroServo << 4) + S0_V_MIN_MAX;
      EEPROM.put(adresse, vitesse);
    }    
  }

  /*
   * retourne la vitesse de max vers min du servo numServo (0 à 7)
   */
  float vitesseServoMaxMin(const byte numeroServo)
  {
    float vitesse;
    if (numeroServo < 8) {
      int adresse = (numeroServo << 4) + S0_V_MAX_MIN;
      long int vitesseBrute;
      EEPROM.get(adresse, vitesseBrute);
      if (vitesseBrute == -1) vitesse = 0.0;
      else EEPROM.get(adresse, vitesse);
    }
    else vitesse = 0.0;
    
    return vitesse;
  }

  /*
   * détermine la vitesse de max vers min du servo numServo (0 à 7)
   */
  void fixeVitesseServoMaxMin(const byte numeroServo, const float vitesse)
  {
   if (numeroServo < 8) {
      int adresse = (numeroServo << 4) + S0_V_MAX_MIN;
      EEPROM.put(adresse, vitesse);
    }    
  }
}

