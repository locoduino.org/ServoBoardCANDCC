/*=======================================================================================
 * Broches.h
 *---------------------------------------------------------------------------------------
 * Logiciel pour la carte 8 servos CAN + DCC
 *
 * LOCODUINO, http://www.locoduino.org
 *
 * Auteur : Jean-Luc B\'echennec
 *
 * Ce logiciel est distribu\'e sous la licence GNU Public v2 (GPLv2)
 *
 * Please read the LICENCE file
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef __BROCHES_H__
#define __BROCHES_H__

/*
 * Sur la carte prototype, le bouton CFG est sur la broche qui sert egalement
 * a l'ACK du DCC. Cela ne fonctionne pas si l'optocoupleur de l'ACK est en place
 * Sur la carte definitive, le bouton CFG est sur la broche qui sert egalement
 * a l'IT DCC, ce qui fonctionne.
 */
#define CFG_SUR_IT_DCC

/*
 * La broche du bouton CFG est partagé avec l'IT DCC ou avec l'ACK DCC
 */
#ifdef CFG_SUR_IT_DCC
static const byte pinBoutonCFG = 2;
#else
static const byte pinBoutonCFG = 4;
#endif

/*
 * Broches pour le DCC
 *
 * Broche d'acquittement DCC, surconsommation de 60mA
 */
static const byte pinACKDCC = 4;
/* Broche d'IT DCC */
static const byte ITDCC = 2;

/*
 * Broches pour le CAN
 *
 * Chip Select du MCP2515 pour la communication SPI
 */
static const byte chipSelectSPI2515 = 10;
/* Interruption CAN */
static const byte ITCAN = 3;

/*
 * Broches pour les servomoteurs
 */
static const byte servo0 = 6;
static const byte servo1 = 7;
static const byte servo2 = 8;
static const byte servo3 = 9;
static const byte servo4 = A0;
static const byte servo5 = A1;
static const byte servo6 = A2;
static const byte servo7 = A3;

/*
 * Table de correspondance numero de servo broche
 */
static const byte brochePourServo [] = {
  servo0, servo1, servo2, servo3, servo4, servo5, servo6, servo7
};

/* LED integree a l'Arduino */
static const byte pinLED = 13;

#endif

